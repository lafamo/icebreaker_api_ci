from flask import Response, url_for, jsonify
from app.main import app

import pytest 
import json 
"""
    Unit tests for flask
"""



#@def client(app):
 #   return app.test_client()

def test_ping(client):
     resp=client.get("/api/ping")
     assert 200 == resp.status_code

def test_content(client):
     resp=client.get("/api")
     assert 10 <=  resp.content_length

def test_response5(client):
     resp= client.get("api/5")
     sentences= json.loads(resp.get_data(as_text=True))
     print(sentences)
     assert 5  ==  len(sentences["data"])

def test_response10(client):
     resp= client.get("api/10")
     sentences= json.loads(resp.get_data(as_text=True))
     print(sentences)
     assert 10  ==  len(sentences["data"])

def test_response15(client):
     resp= client.get("api/15")
     sentences= json.loads(resp.get_data(as_text=True))
     print(sentences)
     assert 15  ==  len(sentences["data"])

def test_integer(client):
     resp= client.get("api/fatima")
     resp= resp.get_data(as_text=True)
     print(resp)
     assert '{"Error":"Must be an integer"}\n' == resp




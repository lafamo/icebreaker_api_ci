FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8
RUN pip install flask pytest
COPY ./app /app
WORKDIR /app
CMD ["pytest"]
